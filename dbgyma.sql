-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-07-2019 a las 19:28:27
-- Versión del servidor: 10.3.16-MariaDB
-- Versión de PHP: 7.2.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbgyma`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `horarioAddOrEdit` (IN `_id_hor` INT, IN `_id_ins` INT, IN `_entrada_ins` TIME, IN `_salida_ins` TIME)  BEGIN
	IF _id_hor = 0 THEN
		INSERT INTO horario (id_ins, entrada_ins, salida_ins)
		VALUES (_id_ins, _entrada_ins, _salida_ins);
		SET _id_hor = LAST_INSERT_ID();
    ELSE
		UPDATE horario
        SET 
			id_ins = _id_ins,
            entrada_ins = _entrada_ins,
            salida_ins = _salida_ins
            WHERE id_hor = _id_hor;
	END IF;
    
    SELECT _id_hor AS id_hor;
			
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `nutricionAddOrEdit` (IN `_id_nut` INT, IN `_titulo_n` VARCHAR(50), IN `_detalle_n` VARCHAR(300), IN `_imagen_n` VARCHAR(150))  BEGIN
	IF _id_nut = 0 THEN
		INSERT INTO nutricion (titulo_n, detalle_n, imagen_n)
		VALUES (_titulo_n, _detalle_n, _imagen_n);
		SET _id_nut = LAST_INSERT_ID();
    ELSE
		UPDATE nutricion
        SET 
			titulo_n = _titulo_n,
            detalle_n = _detalle_n,
            imagen_n = _imagen_n
            WHERE id_nut = _id_nut;
	END IF;
    
    SELECT _id_nut AS _id_nut;
			
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `rutinaAddOrEdit` (IN `_id_rut` INT, IN `_titulo_r` VARCHAR(50), IN `_detalle_r` VARCHAR(300), IN `_imagen_r` VARCHAR(150))  BEGIN
	IF _id_rut = 0 THEN
		INSERT INTO rutina (titulo_r, detalle_r, imagen_r)
		VALUES (_titulo_r, _detalle_r, _imagen_r);
		SET _id_rut = LAST_INSERT_ID();
    ELSE
		UPDATE rutina
        SET 
			titulo_r = _titulo_r,
            detalle_r = _detalle_r,
            imagen_r = _imagen_r
            WHERE id_rut = _id_rut;
	END IF;
    
    SELECT _id_rut AS _id_rut;
			
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `tipo_usuarioAddOrEdit` (IN `_id_tipo` INT, IN `_descripcion` VARCHAR(15))  BEGIN
	IF _id_tipo = 0 THEN
		INSERT INTO tipo_usuario (descripcion)
		VALUES (_descripcion);
		SET _id_tipo = LAST_INSERT_ID();
    ELSE
		UPDATE tipo_usuario
        SET 
			descripcion = _descripcion
            WHERE id_tipo = _id_tipo;
	END IF;
    
    SELECT _id_tipo AS id_tipo;
			
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_horarioAddOrEdit` (IN `_id_usr_hor` INT, IN `_id_cl` INT, IN `_id_hor` INT, IN `_entrada_cl` TIME, IN `_salida_cl` TIME)  BEGIN
	IF _id_usr_hor = 0 THEN
		INSERT INTO horario (id_cl, id_hor, entrada_cl, salida_cl)
		VALUES (_id_cl, _id_hor, _entrada_cl, _salida_cl);
		SET _id_usr_hor = LAST_INSERT_ID();
    ELSE
		UPDATE user_horario
        SET 
			id_cl = _id_cl,
            id_hor = _id_hor,
            entrada_cl = _entrada_cl,
            salida_cl = _salida_cl
            WHERE id_usr_hor = _id_usr_hor;
	END IF;
    
    SELECT _id_usr_hor AS id_usr_hor;
			
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `usuarioAddOrEdit` (IN `_id_usr` INT, IN `_id_tipo` INT, IN `_nombre` VARCHAR(30), IN `_apellido` VARCHAR(30), IN `_correo` VARCHAR(30), IN `_contrasenia` VARCHAR(30), IN `_celular` INT, IN `_formacion` VARCHAR(30), IN `_experiencia` VARCHAR(30), IN `_aptitudes` VARCHAR(30), IN `_sexo` VARCHAR(10), IN `_imagen_u` VARCHAR(150))  BEGIN
	IF _id_usr = 0 THEN
		INSERT INTO usuario (id_tipo, nombre, apellido, correo, contrasenia,
		celular, formacion, experiencia, aptitudes, sexo, imagen_u)
		VALUES (_id_tipo, _nombre, _apellido, _correo, _contrasenia,
		_celular, _formacion, _experiencia, _aptitudes, _sexo,_imagen_u);
		SET _id_usr = LAST_INSERT_ID();
    ELSE
		UPDATE usuario
        SET 
			id_tipo = _id_tipo,
            nombre = _nombre,
			apellido = _apellido,
            correo = _correo,
            contrasenia = _contrasenia,
            celular = _celular, 
            formacion = _formacion,
            experiencia = _experiencia,
            aptitudes = _aptitudes,
            sexo = _sexo,
            imagen_u = _imagen_u
            WHERE idusr = _id_usr;
	END IF;
    
    SELECT _id_usr AS id_usr;
			
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `id_hor` int(11) NOT NULL,
  `id_ins` int(11) NOT NULL,
  `entrada_ins` time NOT NULL,
  `salida_ins` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`id_hor`, `id_ins`, `entrada_ins`, `salida_ins`) VALUES
(1, 2, '12:00:00', '17:00:00'),
(2, 2, '12:00:00', '17:00:00'),
(3, 2, '12:00:00', '17:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nutricion`
--

CREATE TABLE `nutricion` (
  `id_nut` int(11) NOT NULL,
  `titulo_n` varchar(50) NOT NULL,
  `detalle_n` varchar(300) NOT NULL,
  `imagen_n` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nutricion`
--

INSERT INTO `nutricion` (`id_nut`, `titulo_n`, `detalle_n`, `imagen_n`) VALUES
(1, 'Tips 1', 'qwertyuiop', 'asdfghjklñ'),
(2, 'Tips 2', 'qwertyuiop', 'asdfghjklñ'),
(3, 'Tips 3', 'qwertyuiop', 'asdfghjklñ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rutina`
--

CREATE TABLE `rutina` (
  `id_rut` int(11) NOT NULL,
  `titulo_r` varchar(50) NOT NULL,
  `detalle_r` varchar(300) NOT NULL,
  `imagen_r` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rutina`
--

INSERT INTO `rutina` (`id_rut`, `titulo_r`, `detalle_r`, `imagen_r`) VALUES
(1, 'Rutina 1', 'qwertyuiop', 'asdfghjklñ'),
(2, 'Rutina 2', 'qwertyuiop', 'asdfghjklñ'),
(3, 'Rutina 3', 'qwertyuiop', 'asdfghjklñ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipo` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo`, `descripcion`) VALUES
(1, 'Admin'),
(2, 'Instructor'),
(3, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_horario`
--

CREATE TABLE `user_horario` (
  `id_usr_hor` int(11) NOT NULL,
  `id_cl` int(11) NOT NULL,
  `id_hor` int(11) NOT NULL,
  `entrada_cl` time NOT NULL,
  `salida_cl` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_horario`
--

INSERT INTO `user_horario` (`id_usr_hor`, `id_cl`, `id_hor`, `entrada_cl`, `salida_cl`) VALUES
(1, 3, 2, '09:00:00', '10:00:00'),
(2, 3, 2, '09:00:00', '10:00:00'),
(3, 3, 2, '09:00:00', '10:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usr` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `apellido` varchar(30) NOT NULL,
  `correo` varchar(30) NOT NULL,
  `contrasenia` varchar(20) NOT NULL,
  `celular` int(11) NOT NULL,
  `formacion` varchar(200) NOT NULL,
  `experiencia` varchar(200) NOT NULL,
  `aptitudes` varchar(200) NOT NULL,
  `sexo` varchar(10) NOT NULL,
  `imagen_u` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usr`, `id_tipo`, `nombre`, `apellido`, `correo`, `contrasenia`, `celular`, `formacion`, `experiencia`, `aptitudes`, `sexo`, `imagen_u`) VALUES
(1, 1, 'David', 'Castro', 'maria@gmail.com', '12345', 987654321, 'completa', 'mucha', 'varias', 'hombre', 'asdfghjklñ'),
(2, 2, 'David', 'Castro', 'maria@gmail.com', '12345', 987654321, 'completa', 'mucha', 'varias', 'hombre', 'asdfghjklñ'),
(3, 3, 'David', 'Castro', 'maria@gmail.com', '12345', 987654321, 'completa', 'mucha', 'varias', 'hombre', 'asdfghjklñ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`id_hor`),
  ADD KEY `id_ins` (`id_ins`);

--
-- Indices de la tabla `nutricion`
--
ALTER TABLE `nutricion`
  ADD PRIMARY KEY (`id_nut`);

--
-- Indices de la tabla `rutina`
--
ALTER TABLE `rutina`
  ADD PRIMARY KEY (`id_rut`);

--
-- Indices de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indices de la tabla `user_horario`
--
ALTER TABLE `user_horario`
  ADD PRIMARY KEY (`id_usr_hor`),
  ADD KEY `id_cl` (`id_cl`),
  ADD KEY `id_hor` (`id_hor`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usr`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `id_hor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `nutricion`
--
ALTER TABLE `nutricion`
  MODIFY `id_nut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `rutina`
--
ALTER TABLE `rutina`
  MODIFY `id_rut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tipo_usuario`
--
ALTER TABLE `tipo_usuario`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `user_horario`
--
ALTER TABLE `user_horario`
  MODIFY `id_usr_hor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usr` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `horario`
--
ALTER TABLE `horario`
  ADD CONSTRAINT `horario_ibfk_1` FOREIGN KEY (`id_ins`) REFERENCES `usuario` (`id_usr`);

--
-- Filtros para la tabla `user_horario`
--
ALTER TABLE `user_horario`
  ADD CONSTRAINT `user_horario_ibfk_1` FOREIGN KEY (`id_cl`) REFERENCES `usuario` (`id_usr`),
  ADD CONSTRAINT `user_horario_ibfk_2` FOREIGN KEY (`id_hor`) REFERENCES `horario` (`id_hor`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_usuario` (`id_tipo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
